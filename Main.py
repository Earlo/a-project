# coding: utf-8
#This is the main file. Run this, to run the program

import random
import pygame, sys
from pygame.locals import *
import math

import Object
import Rooms
import Item
import Factions
import Buildings

#Setup
pygame.init()

#colors
black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)

# set up fonts
basicFont = pygame.font.SysFont(None, 48)

FPS = 60 # 60 frames per second
clock = pygame.time.Clock()

#window
SWIDTH = 800
SHEIGTH = 640
global MainWindow
MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))

room = Rooms.room0()
#room = [Object.Tile(x,y,0) for x in range(30) for y in range(30)]
for tile in room:
	tile.set_tile()

#To change the amount of units in each team, change the value inside parantheses after 'range'
Team_1 = Factions.team('Team1')
Team_2 = Factions.team('Team2')
Team_1.set_team([Object.Unit(1,x+10) for x in range(4)])
Team_2.set_team([Object.Unit(28,18-x) for x in range(4)])
Team_1.set_team([Buildings.Base(1,x+1) for x in range(1)])
Team_2.set_team([Buildings.Base(27,27-x) for x in range(1)])

for unit in Team_1.units:
	unit.set_team(red,Item.Sword())
for unit in Team_2.units:
	unit.set_team(blue,Item.Robofist())

effects = []

done = False
while not done:
	for event in pygame.event.get(): # User did something
		if event.type == pygame.QUIT: # Closed from X
			done = True # Stop the Loop
	
	MainWindow.fill(white)

	for tile in room:
		tile.draw()
		
	for unit in Team_1.units:
		unit.update()
	for unit in Team_2.units:
		unit.update()
	for effect in effects:
		effect.draw()
		if effect.duration<= 0:
			effects.remove(effect)
		
	pygame.display.flip()
	clock.tick(FPS)
	pygame.display.set_caption("FPS: %i" % clock.get_fps())
pygame.quit()

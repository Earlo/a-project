#This file contains all preset items used by creatures in Object.py

import math

class Sword(object):
	def __init__(self):
		self.name = 'a sword'
		self.dmgtype = ['slash','pierce']
		self.damage = 5
#~~~~~~~~~~~~End of Sword~~~~~~~~~~~~~~
class Robofist(object):
	def __init__(self):
		self.name = 'a robotfist'
		self.dmgtype = ['blunt']
		self.damage = 7
#~~~~~~~~~~~End of Robofist~~~~~~~

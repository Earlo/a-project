#This file contain all creatures and stuff. yeah, sorry, i know. It's really pathetic to try to make a game or something like that. I won't judge you one bit if you hate me. it's ok. : )

import random
import pygame, sys
import math

import AI
import Body
import Item
import Movement
import Names

black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)

class Unit (object):
	def __init__(self,x,y):
		self.type = 'Unit'
		self.name = Names.rname()
		self.body = Body.Humanoid()
		self.start_pos = [x,y]
		self.mission = 'attack_closest'
		self.speed = 5 + random.randint(-3,3)
		self.strength = 5 + random.randint(-3,3)
		self.maxhp= self.strength*10 + 15
		self.hp = self.maxhp
		self.action_points = 0
		self.next_to_enemy = 0
		self.target = None
		from Main import room
		for tile in room:
			if tile.pos == self.start_pos:
				self.tile = tile
				self.start_tile = tile
				self.tile.occupied = 1

	def set_team(self,colour,wpn):
		self.weapon = wpn
		self.colour = colour
		self.enemy = self.team.enemy


		
	def update(self):
		if self.hp <= (self.maxhp/2):
			self.mission='heal_self'
		if self.hp <= 0:
			print (self.name + ' dies!' + '\n')
			self.tile.occupied = 0
			self.tile = self.start_tile
			self.tile.occupied = 1
			self.action_points = 0
			self.target = None
			self.hp= self.maxhp
			
		self.action_points += self.speed
		if self.mission == 'attack_closest':
			AI.attack_closest(self)
		if self.mission == 'heal_self':
			AI.heal_self(self)
			
		from Main import MainWindow	
		pygame.draw.circle(MainWindow,self.colour,(self.tile.pos[0]*20+10,self.tile.pos[1]*20+10),5)
		x = round(20*self.hp/self.maxhp)
		pygame.draw.rect(MainWindow,red,(self.tile.pos[0]*20+20,self.tile.pos[1]*20,-20+x,2),0)
		pygame.draw.rect(MainWindow,green,(self.tile.pos[0]*20,self.tile.pos[1]*20, x,2),0)
		
#~~~~~~~~~~~~~~~~~~~~~~~~~Unit end~~~~~~~~~~~~~~~~~~~~~

		
		

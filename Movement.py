#This file contains methods to calculate different kinds of movement

import math
import time
import pygame


def simple_distance(pos1,pos2):
    return (pos1[0]-pos2[0])**2 + (pos1[1]-pos2[1])**2
    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vector_distance (pos,tar):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	return math.fabs(math.sqrt(vecx**2+vecy**2))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vector_movement(pos,tar):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	length = math.fabs(math.sqrt(vecx**2+vecy**2))
	if length == 1 or length <= 1.5:
		return (pos[0],pos[1],1) 
	unitvector = (vecx/length, vecy/length)
	return (int(round (unitvector[0])), int (round (unitvector[1])),0)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def hector_movement(pos,tar):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	length = math.fabs(math.sqrt(vecx**2+vecy**2))
	if length == 1 or length == math.sqrt(2):
		return (pos[0],pos[1],1) 
	unitvector = (vecx/length, vecy/length)
	newx = int (round (pos[0] + unitvector[0],0))
	newy = int (round (pos[1] + unitvector[1],0))
	return (newx,newy,0)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def mark_path(node):
	if node.parent == 0:
		node.occupied = 4
	else:
		node.occupied = 4
		mark_path(node.parent)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def getdircost(loc1,loc2):    
	if loc1[0] - loc2[0] != 0 and loc1[1] - loc2[1] != 0:
		return 14 # diagnal movement
	else:
		return 10 # horizontal/vertical movement 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def get_h_score(end,current):
	hscore = (abs(end[0]-current[0])+abs(end[1]-current[1])) * 10
	return hscore
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
def create_path(end, start, grid, mover):
	openset = [] # The set of tentative nodes to be evaluated, initially containing the start node
	closedset = []
	start.parent = None
	# Cost from start along best known path.
	start.gscore = 0
	start.fscore = start.gscore + (abs(end.pos[0]-start.pos[0])+abs(end.pos[1]-start.pos[1])) * 10

		
	openset.append(start)
	current = start
	done = False
	while not done:
		closedset.append(current)
		if current == end:
			done = True
			return current.parent
		openset.remove(current)
		for n in range(len(current.next)):
			if not current.next[n] == None:
				if not current.next[n] in closedset:
					if not current.next[n].occupied >= 1:
						if not current.next[n] in openset:	
							current.next[n].parent = current
							openset.append(current.next[n])
							current.next[n].gscore = current.next[n].movcost * getdircost(current.next[n].pos,current.pos) + current.gscore
							current.next[n].hscore = get_h_score(current.next[n].pos,end.pos)
							current.next[n].fscore = current.next[n].gscore+current.next[n].hscore
						else:
							if current.next[n].gscore > current.next[n].movcost * getdircost(current.next[n].pos,current.pos) + current.gscore:
								current.next[n].parent = current
		if len(openset) == 0:
			#no room to move, sorry
			mover.action_points +=75
			return end												
		current = min (openset,key=lambda node:node.fscore)


#This file is to calculate combat events and messages
import math
import random



reactionadjective = (' fails to block ',' barely blocks ',' blocks ',' calmly blocks ',' gracefully parries ')
attackadjective = (' pathetic ',' weak ',' ',' skillful ',' Masterful ')

def Attack(attacker):
	combat_message = []
	#rolls
	Aspd = attacker.speed + random.randint(-3,3)
	Dspd = attacker.target.speed + random.randint(-3,3)
	Astr = attacker.strength 
	Dstr = attacker.target.strength
	dmg = attacker.weapon.damage
	
	#decide the attack type attacker uses
	if len(attacker.weapon.dmgtype) == 1:
		dmgtype = attacker.weapon.dmgtype[0]
	else:
		dmgtype = attacker.weapon.dmgtype[random.randint(0,len(attacker.weapon.dmgtype)-1)]
		
	if Aspd >= 8:
		charge = 1
		Astr += 3
		print (attacker.name +" charges at " +attacker.target.name)
	else:
		charge = 0
		print (attacker.name +" attacks " +attacker.target.name)
	
	speedifference = Dspd - Aspd
	if speedifference <= -2:
		# Defender has no time to do anything
		RA = 0
		Dstr = 0
	elif speedifference == -1:
		#Defender has trouble bloking
		RA = 1
		Dstr += random.randint(-3,3)
		Dstr = round(Dstr*0.5,0)
	elif speedifference == 0:
		#Even speed, fair hit and block
		RA = 2
		Dstr += random.randint(-3,3)
	elif speedifference == 1:
		# Extraordinary block
		RA = 3
		Dstr += random.randint(-3,3)
		Dstr = Dstr*2
	elif speedifference >= 2:
		# calm parry
		RA = 4
		Dstr += random.randint(-3,3)
		Dstr = Dstr*4
		
	#attack strength 	
	Astr += random.randint(-3,3)
	if Astr >= 10:
		SA = 4
	elif 7 <= Astr <= 9:
		SA = 3
	elif 5 <= Astr <= 6:
		SA = 2
	elif 2 <= Astr <= 4:
		SA = 1
	elif Astr <= 1:
		SA = 0
		
	strengthdifference = Dstr - Astr
	dmg += - strengthdifference

			
	#message
	#print (str(Astr) + " STR and " + str(Aspd) + " SPD vs " + str(Dstr) + " STR and " + str(Dspd) + " SPD") #For debug use
	
	print (attacker.target.name + reactionadjective[RA] + attacker.name + "s" + attackadjective[SA] + (dmgtype) + " attack ")
	
	if dmg < 0:
		dmg = 0	
	elif dmg > 8:
		if dmgtype == 'slash':
			if (random.randint(0,5) == 5):
				if (random.randint(0,5) == 5):
					if (random.randint(0,5) == 5):
						print (attacker.name + " cuts " + attacker.target.name + "s " + attacker.target.body.major_limb[random.randint(0,4)] + " with " + attacker.weapon.name + ", severed part sails off in arc")
						dmg += 50
					else:
						print (attacker.name + " cuts " + attacker.target.name + "s " + attacker.target.body.normal_limb[random.randint(0,3)] + " with " + attacker.weapon.name + ", severed part sails off in arc")
						dmg += 15
				else:
					print (attacker.name + " cuts " + attacker.target.name + "s " + attacker.target.body.minor_limb[random.randint(0,9)] + " with " + attacker.weapon.name + ", severed part sails off in arc")
					dmg += 5
		elif dmgtype == 'blunt':
			if (random.randint(0,5) == 5):
				dmg += 5
	
	print (str(dmg) + " points of damage have been dealt to " + attacker.target.name + "\n")
	return dmg


	

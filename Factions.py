

class team(object):
	def __init__(self,name):
		self.name = name
		self.units = []
		self.buildings = []
		
	def set_team(self,obj):
		#teamporary solution
		from Main import Team_1
		from Main import Team_2
		if Team_1 == self:
			self.enemy = Team_2
		else:
			self.enemy = Team_1
		for instance in obj:
			if instance.type == 'Unit':
				self.units.append(instance)
				instance.team = self
			elif instance.type == 'Building':
				self.buildings.append(instance)
	def draw(self):
		for unit in self.units:
			unit.update()
		for building in self.buildings:
			building.update()
		

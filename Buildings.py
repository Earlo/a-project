
black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
class Base(object):
	def __init__(self,x,y):
		self.type = 'Building'
		self.pos = [x,y]
		self.hp = 100
		self.maxhp = 100
		self.tiles =[]
		from Main import room
		for tile in room:
			if tile.pos == self.pos:
				self.tiles.append(tile)
				self.tiles.append(tile.next[3])
				self.tiles.append(tile.next[6])
				self.tiles.append(tile.next[2])
		for tile in self.tiles:
			tile.colour = cyan
			tile.line = 0
		
	def set_team(self,colour):
		self.colour = colour

	def update(self):
		if self.hp <= 0:
			print (self.name + ' dies!' + '\n')

		from Main import MainWindow	
		x = round(40*self.hp/self.maxhp)
		pygame.draw.rect(MainWindow,red,(self.tile.pos[0]*20+20,self.tile.pos[1]*20,-40+x,3),0)
		pygame.draw.rect(MainWindow,green,(self.tile.pos[0]*20,self.tile.pos[1]*20, x,3),0)		
#~~~~~~~~~~~~~~~~~~~~~~~~~~Base end~~~~~~~~~~~~~~~~~~~~~~

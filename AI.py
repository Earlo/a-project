#Yeah, sorry no real inteligence here, just few calculations to keep the units doing somethingsomething

import Movement
import Combat
import random
import Effect

def attack_closest(mover):
	if mover.action_points >= 50:
		mover.target = min (mover.enemy.units,key=lambda target:Movement.vector_distance(mover.tile.pos,target.tile.pos))
		distance = Movement.vector_distance(mover.tile.pos,mover.target.tile.pos)
		if distance == 1 or distance <= 1.5:
			mover.next_to_enemy=True
		else:
			mover.next_to_enemy=False
		if mover.next_to_enemy == False:
			from Main import room
			mover.action_points -= 50
			mover.tile.occupied = 0
			current_pos = mover.tile.width
			mover.tile = Movement.create_path(mover.tile, mover.target.tile, room, mover)
			mover.tile.occupied = 1
			from Main import effects
			effects.append(Effect.movement(mover.tile.width,current_pos))	
		if mover.next_to_enemy == True:
			if mover.action_points >= 100:
				mover.action_points -= 100
				mover.target.hp -= (Combat.Attack(mover))
				from Main import effects
				effects.append(Effect.attack(mover.tile.width, mover.target.tile.width,))
				
def heal_self(mover):
	if mover.action_points >= 50:
		mover.tile.occupied = 0
		heal_tiles = []
		for building in mover.team.buildings:
			for tile in building.tiles:
				if tile.occupied == 0:
					heal_tiles.append(tile)
		mover.target = min (heal_tiles,key=lambda target:Movement.vector_distance(mover.tile.pos,target.pos))
		distance = Movement.vector_distance(mover.tile.pos,mover.target.pos)
		if distance == 0:
			mover.next_to_enemy=True


		else:
			mover.next_to_enemy=False
		if mover.next_to_enemy == False:
			from Main import room
			mover.action_points -= 50
			mover.tile.occupied = 0
			current_pos = mover.tile.width
			mover.tile = Movement.create_path(mover.tile, mover.target, room, mover)
			mover.tile.occupied = 1
			from Main import effects
			effects.append(Effect.movement(mover.tile.width,current_pos))	
		if mover.next_to_enemy == True:
			mover.action_points -= 50
			mover.hp += 10
			if mover.hp >= mover.maxhp:
				mover.hp = mover.maxhp
				mover.mission='attack_closest'

		mover.tile.occupied = 1
				


import random
import pygame, sys
import math
import Movement


black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)

class attack(object):
	def __init__(self,start_pos,end_pos):

		self.start_pos = [start_pos[0]+10,start_pos[1]+10]
		self.end_pos = [end_pos[0]+10,end_pos[1]+10]
		self.duration = 10
	def draw(self):
		self.duration-=1
		from Main import MainWindow
		pygame.draw.line(MainWindow,black,self.start_pos,self.end_pos,1)
		pygame.draw.circle(MainWindow,black,self.end_pos,2)
		
		
class movement(object):
	def __init__(self,start_pos,end_pos):

		self.start_pos = [start_pos[0]+10,start_pos[1]+10]
		self.end_pos = [end_pos[0]+10,end_pos[1]+10]
		if Movement.vector_distance(self.start_pos,self.end_pos) > 30:
			self.duration = 100
		else:
			self.duration = 5
	def draw(self):
		self.duration-=1
		from Main import MainWindow
		pygame.draw.line(MainWindow,red,self.start_pos,self.end_pos,1)
		pygame.draw.circle(MainWindow,red,self.end_pos,2)


